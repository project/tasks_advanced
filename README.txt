

MODULE
------
Tasklist Advanced


DESCRIPTION
-----------
The 'Tasklist Advanced' module extends the existing Tasklist module, and adds
categorization, additional views, and filtering based on "Getting Things Done"
by David Allen. It requires, and depends on the original 'Tasklist' module, and
is fully backward compatible. For attaching dates to tasks, use the 'Event' module.


FEATURES
--------
(In addition to all Tasklist features)

- Additional views
  - my tasks
  - all tasks, grouped by user

- Categorization based on "Getting Things Done" by David Allen

- Form or URL based filtering by
  Note: for advanced URL based filtering try this approach:
  tasks/user/1?status=1&taskcategory=unknown,next&tasktype=action
  - Status
  - Category
  - Task type

- Tagging of tasks as action (default) or project (multi-action task)

- Hierarchical tree listing


REQUIREMENTS
------------
- Drupal 5.0
- MySQL as your database engine (for .install file to create required database table)
- Tasklist module [tasks.module]
- Event module [event.module] (optional)


INSTALLATION
------------
1. Install the tasks.module. (IMPORTANT)
   Note: colour code tasks by user by going to user/[userid]/edit and changing the
   tasklist colour for that user.

2. Copy the tasks_advanced folder and its contents to the Drupal modules directory.

3. Go to 'Administer -> Site building -> Modules' and enable tasks_advanced.
   Note: for each module that you enable, all necessary database tables will
   be automatically created, using Drupal's new .install file system (mysql only).

4. Go to 'Administer -> User management -> Access control' to configure access
   privileges for this module.

5. Depending on your permissions the new menu options 'Tasks' and 'Tasks -> My tasks'
   or only 'My tasks' should appear.

6. Optional: Add dates through event module.

   a. Enable event module (you don't need any of the other modules bundled with event)

   b. Browse to /admin/content/types/tasks

   c. At the bottom of the page you'll see an option to "Show in event calendar", which you set to "All views" or "Only in views for this type"


CREDITS
-------
Authored and maintained by Balarama Bosch (moonray)
Sponsored by Krishna.com
